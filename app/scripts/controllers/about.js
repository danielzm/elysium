'use strict';

/**
 * @ngdoc function
 * @name elysiumApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the elysiumApp
 */
angular.module('elysiumApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
