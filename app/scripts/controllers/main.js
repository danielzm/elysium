/* global $, WOW */
'use strict';

/**
 * @ngdoc function
 * @name elysiumApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the elysiumApp
 */

angular.module('elysiumApp')
    .controller('MainCtrl', function () {
        $(document).ready(function () {
            new WOW().init();
            /*
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };
            */
            $().UItoTop({
                easingType: 'easeOutQuart'
            });

            var pull = $('#pull'),
                menu = $('nav ul');

            $(pull).on('click', function (e) {
                e.preventDefault();
                menu.slideToggle();
            });
            $(window).resize(function () {
                var w = $(window).width();
                if (w > 320 && menu.is(':hidden')) {
                    menu.removeAttr('style');
                }
            });
            // End-top-nav-script

            $('.scroll').click(function (event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });

            // hide nav bar
            setTimeout(function () {
                window.scrollTo(0, 1);
            }, 0);
        });
    });
