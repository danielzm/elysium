'use strict';

/**
 * @ngdoc function
 * @name elysiumApp.controller:CatalogCtrl
 * @description
 * # CatalogCtrl
 * Controller of the elysiumApp
 */
angular.module('elysiumApp')
  .controller('CatalogCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
