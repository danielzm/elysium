'use strict';

/**
 * @ngdoc overview
 * @name elysiumApp
 * @description
 * # elysiumApp
 *
 * Main module of the application.
 */
angular
    .module('elysiumApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })
            .when('/catalog', {
                templateUrl: 'views/catalog.html',
                controller: 'CatalogCtrl'
            })
            .when('/catalog_bronce', {
                templateUrl: 'views/catalog_bronce.html',
                controller: 'CatalogBronceCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
